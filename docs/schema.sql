CREATE TABLE `users` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `password` varchar(255),
  `username` varchar(255),
  `first` varchar(255),
  `last` varchar(255),
  `email` varchar(255),
  `active` bool DEFAULT 1,
  `created_at` timestamp DEFAULT CURRENT_TIMESTAMP
);
